from selenium import webdriver

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('headless')
chrome_options.add_argument('no-sandbox')
 
driver = webdriver.Chrome(executable_path ='/usr/lib/chromium-browser/chromedriver',chrome_options=chrome_options)
driver.get('https://the-internet.herokuapp.com/context_menu')
html = driver.page_source
assert "Right-click in the box below to see one called 'the-internet'" in html
print("the string --- Right-click in the box below to see one called 'the-internet' ---- found on page")